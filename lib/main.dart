import 'package:coronavirus_bloc/app_localizations.dart';
import 'package:coronavirus_bloc/bloc/summary_bloc.dart';
import 'package:coronavirus_bloc/repositories/covid19_api_client.dart';
import 'package:coronavirus_bloc/repositories/repositories.dart';
import 'package:coronavirus_bloc/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:coronavirus_bloc/simple_bloc_observer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart' as http;

void main() {
  Bloc.observer = SimpleBlocObserver();
  final COVID19Repository covid19repository =
      COVID19Repository(apiClient: COVID19ApiClient(httpClient: http.Client()));
  runApp(MyApp(covid19repository: covid19repository));
}

class MyApp extends StatelessWidget {
  final COVID19Repository covid19repository;

  MyApp({Key key, @required this.covid19repository})
      : assert(covid19repository != null),
        super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: [
        Locale('en', 'US'),
        Locale('ru', 'RU'),
        Locale('kk', 'KZ')
      ],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      title:
          'COVID19', //AppLocalizations.of(context).translate(I18nKeys.appTitle),
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (_) => SummaryBloc(covid19repository: covid19repository)
          ..add(SummaryRequested()),
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: SummaryPage(),
        ),
      ),
    );
  }
}
