import 'dart:convert';

import 'package:coronavirus_bloc/models/models.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class COVID19ApiClient {
  static const baseUrl = 'https://api.covid19api.com';
  final http.Client httpClient;

  COVID19ApiClient({@required this.httpClient}) : assert(httpClient != null);

  Future<Summary> fetchSummary() async {
    print('fetchSummary called');
    final url = '$baseUrl/summary';
    final response = await this.httpClient.get(url);
    if (response.statusCode != 200) {
      throw Exception('${response.statusCode} - ${response.reasonPhrase}');
    }
    final json = jsonDecode(response.body);
    return Summary.fromJson(json);
  }

  Future<List<CountryDetails>> fetchByCountry(String slug) async {
    print('fetchByCountry called');
    final url = '$baseUrl/live/country/$slug';
    final response = await this.httpClient.get(url);
    if (response.statusCode != 200) {
      throw Exception('${response.statusCode} - ${response.reasonPhrase}');
    }
    final json = jsonDecode(response.body);
    List<CountryDetails> list = List<CountryDetails>();
    for (var item in json) {
      list.add(CountryDetails.fromJson(item));
    }
    return list;
  }
}
