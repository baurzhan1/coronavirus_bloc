import 'package:coronavirus_bloc/models/models.dart';
import 'package:coronavirus_bloc/repositories/covid19_api_client.dart';
import 'package:flutter/material.dart';

class COVID19Repository {
  final COVID19ApiClient apiClient;

  COVID19Repository({@required this.apiClient}) : assert(apiClient != null);

  Future<Summary> getSummary() {
    return apiClient.fetchSummary();
  }

  Future<List<CountryDetails>> getByCountry(String slug) {
    return apiClient.fetchByCountry(slug);
  }
}
