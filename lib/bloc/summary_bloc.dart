import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:coronavirus_bloc/repositories/repositories.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:coronavirus_bloc/models/models.dart';
part 'summary_event.dart';
part 'summary_state.dart';

class SummaryBloc extends Bloc<SummaryEvent, SummaryState> {
  SummaryBloc({@required this.covid19repository})
      : assert(covid19repository != null),
        super(SummaryInitial());
  final COVID19Repository covid19repository;

  @override
  Stream<SummaryState> mapEventToState(
    SummaryEvent event,
  ) async* {
    if (event is SummaryRequested) {
      print('mapEventToState: SummaryRequested');
      yield SummaryLoadInProgress();
      try {
        final Summary summary = await covid19repository.getSummary();
        yield SummaryLoadSuccess(summary: summary);
      } catch (e) {
        print('mapEventToState:$e');
        yield SummaryLoadFailure(message: e.toString());
      }
    }
    if (event is DetailRequested) {
      print('mapEventToState: DetailRequested');
      yield SummaryLoadInProgress();
      try {
        final List<CountryDetails> list =
            await covid19repository.getByCountry(event.slug);
        yield DetailLoadSuccess(countryName: list[0].country, list: list);
      } catch (e) {
        print('mapEventToState:$e');
        yield SummaryLoadFailure(message: e.toString());
      }
    }
  }
}
