part of 'summary_bloc.dart';

abstract class SummaryState extends Equatable {
  const SummaryState();

  @override
  List<Object> get props => [];
}

class SummaryInitial extends SummaryState {}

class SummaryLoadInProgress extends SummaryState {}

class SummaryLoadSuccess extends SummaryState {
  final Summary summary;

  const SummaryLoadSuccess({@required this.summary}) : assert(summary != null);

  @override
  List<Object> get props => [summary];
}

class SummaryLoadFailure extends SummaryState {
  final String message;

  const SummaryLoadFailure({@required this.message}) : assert(message != null);

  @override
  List<Object> get props => [message];
}

class DetailLoadSuccess extends SummaryState {
  final List<CountryDetails> list;
  final String countryName;

  const DetailLoadSuccess({@required this.list, @required this.countryName})
      : assert(list != null && countryName != null);

  @override
  List<Object> get props => [list, countryName];
}
