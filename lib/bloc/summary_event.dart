part of 'summary_bloc.dart';

abstract class SummaryEvent extends Equatable {
  const SummaryEvent();

  @override
  List<Object> get props => [];
}

class SummaryRequested extends SummaryEvent {}

class DetailRequested extends SummaryEvent {
  final String slug;

  DetailRequested({@required this.slug}) : assert(slug != null);

  @override
  List<Object> get props => [slug];
}
