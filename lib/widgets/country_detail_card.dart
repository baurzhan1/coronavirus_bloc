import 'package:coronavirus_bloc/app_localizations.dart';
import 'package:coronavirus_bloc/models/models.dart';
import 'package:flutter/material.dart';
import '../custom_color_scheme.dart';

class CountryDetailCard extends StatelessWidget {
  final CountryDetails data;

  const CountryDetailCard({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      shadowColor: Colors.grey,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
              tileColor: Colors.blue,
              title: Text(
                data.name,
                textAlign: TextAlign.left,
                style: TextStyle(
                    backgroundColor: Colors.transparent,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0),
              )),
          Padding(
              padding: EdgeInsets.all(10.0),
              child: Table(
                columnWidths: {
                  0: FlexColumnWidth(3),
                  1: FlexColumnWidth(2),
                  2: FlexColumnWidth(3),
                  3: FlexColumnWidth(2)
                },
                children: [
                  TableRow(children: [
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.confirmed)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                    Text(data.confirmed.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.active)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                    Text(data.active.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                  ]),
                  TableRow(children: [
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.deaths)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.deaths,
                        )),
                    Text(data.deaths.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.deaths,
                        )),
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.recovered)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.recovered,
                        )),
                    Text(data.recovered.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.recovered,
                        )),
                  ]),
                ],
              )),
        ],
      ),
    );
  }
}
