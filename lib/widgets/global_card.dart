import 'package:coronavirus_bloc/app_localizations.dart';
import 'package:coronavirus_bloc/models/models.dart';
import 'package:flutter/material.dart';
import '../custom_color_scheme.dart';

class GlobalCard extends StatelessWidget {
  final Global data;

  const GlobalCard({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      shadowColor: Colors.grey,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
              tileColor: Colors.blue,
              title: Text(
                AppLocalizations.of(context).translate(I18nKeys.globalTitle),
                textAlign: TextAlign.left,
                style: TextStyle(
                    backgroundColor: Colors.transparent,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0),
              )),
          Padding(
              padding: EdgeInsets.all(10.0),
              child: Table(
                columnWidths: {
                  0: FlexColumnWidth(3),
                  1: FlexColumnWidth(2),
                  2: FlexColumnWidth(3),
                  3: FlexColumnWidth(2)
                },
                children: [
                  TableRow(children: [
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.newConfirmed)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                    Text(data.newConfirmed.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.totalConfirmed)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                    Text(data.totalConfirmed.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.confirmed,
                        )),
                  ]),
                  TableRow(children: [
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.newDeaths)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.deaths,
                        )),
                    Text(data.newDeaths.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.deaths,
                        )),
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.totalDeaths)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.deaths,
                        )),
                    Text(data.totalDeaths.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.deaths,
                        )),
                  ]),
                  TableRow(children: [
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.newRecovered)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.recovered,
                        )),
                    Text(data.newRecovered.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.recovered,
                        )),
                    Text(
                        '${AppLocalizations.of(context).translate(I18nKeys.totalRecovered)}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.recovered,
                        )),
                    Text(data.totalRecovered.toString(),
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.recovered,
                        )),
                  ])
                ],
              )),
        ],
      ),
    );
  }
}
