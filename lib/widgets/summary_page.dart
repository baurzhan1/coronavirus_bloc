import 'package:coronavirus_bloc/app_localizations.dart';
import 'package:coronavirus_bloc/bloc/summary_bloc.dart';
import 'package:coronavirus_bloc/widgets/country_card.dart';
import 'package:coronavirus_bloc/widgets/global_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'country_detail_card.dart';

class SummaryPage extends StatelessWidget {
  SummaryPage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(
            AppLocalizations.of(context).translate(I18nKeys.summaryTitle),
            style: TextStyle(color: Theme.of(context).colorScheme.onSecondary),
          ),
          //AppLocalizations.of(context).translate(I18nKeys.summaryTitle)),
          backgroundColor: Theme.of(context).colorScheme.secondaryVariant,
        ),
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                        Colors.blue.withOpacity(0.2), BlendMode.dstATop),
                    image: AssetImage('assets/corona.png'))),
            child: BlocBuilder<SummaryBloc, SummaryState>(
                builder: (context, state) {
              if (state is SummaryInitial || state is SummaryLoadInProgress) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is SummaryLoadFailure) {
                print('State:${state.runtimeType}');
                return Center(
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          'state${state.message}',
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Theme.of(context).colorScheme.error),
                        )));
              }
              if (state is SummaryLoadSuccess) {
                if (state.summary == null) {
                  return Center(
                      child: Text(AppLocalizations.of(context)
                          .translate(I18nKeys.noData)));
                }
                return Column(
                  children: <Widget>[
                    GlobalCard(data: state.summary.global),
                    ListTile(
                        tileColor:
                            Theme.of(context).colorScheme.secondaryVariant,
                        title: Text(
                          AppLocalizations.of(context)
                              .translate(I18nKeys.countriesTitle),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              backgroundColor: Colors.transparent,
                              color: Theme.of(context).colorScheme.onSecondary,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0),
                        )),
                    Expanded(
                        child: ListView.builder(
                            itemBuilder: (BuildContext context, int index) {
                              return CountryCard(
                                  data: state.summary.countries[index]);
                            },
                            itemCount: state.summary.countries.length))
                  ],
                );
              }
              if (state is DetailLoadSuccess) {
                if (state.list == null || state.list.isEmpty) {
                  return Center(
                      child: Text(AppLocalizations.of(context)
                          .translate(I18nKeys.noData)));
                }
                return Column(children: [
                  ListTile(
                      tileColor: Theme.of(context).colorScheme.secondaryVariant,
                      title: Text(
                        state.countryName,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            backgroundColor: Colors.transparent,
                            color: Theme.of(context).colorScheme.onSecondary,
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0),
                      )),
                  Expanded(
                      child: ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                            return CountryDetailCard(data: state.list[index]);
                          },
                          itemCount: state.list.length))
                ]);
              }
              return Text(
                  AppLocalizations.of(context).translate(I18nKeys.noData));
            })),
        floatingActionButton:
            BlocBuilder<SummaryBloc, SummaryState>(builder: (context, state) {
          bool visible = true;
          if (state is SummaryLoadInProgress) {
            visible = false;
          }
          Icon icon = Icon(Icons.refresh);
          String tooltip = 'Reload';
          if (state is DetailLoadSuccess) {
            icon = Icon(Icons.arrow_back);
            tooltip = 'Return back';
          }
          return Visibility(
              visible: visible,
              child: FloatingActionButton(
                onPressed: () => BlocProvider.of<SummaryBloc>(context)
                    .add(SummaryRequested()),
                tooltip: tooltip,
                child: icon,
                backgroundColor: Theme.of(context).colorScheme.secondaryVariant,
              ));
        }));
  }
}
