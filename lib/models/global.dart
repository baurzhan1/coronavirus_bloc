part of models;

class Global extends Equatable {
  static const String title = "Global";
  const Global(
      {@required this.newConfirmed,
      @required this.newDeaths,
      @required this.newRecovered,
      @required this.totalConfirmed,
      @required this.totalDeaths,
      @required this.totalRecovered});

  final int newConfirmed;
  final int totalConfirmed;
  final int newDeaths;
  final int totalDeaths;
  final int newRecovered;
  final int totalRecovered;

  @override
  List<Object> get props => [
        newConfirmed,
        totalConfirmed,
        newDeaths,
        totalDeaths,
        newRecovered,
        totalRecovered
      ];

  factory Global.fromJson(dynamic json) {
    return Global(
        newConfirmed: json['NewConfirmed'],
        newDeaths: json['NewDeaths'],
        newRecovered: json['NewRecovered'],
        totalConfirmed: json['TotalConfirmed'],
        totalDeaths: json['TotalDeaths'],
        totalRecovered: json['TotalRecovered']);
  }
}
