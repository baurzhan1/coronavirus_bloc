part of models;

class Summary extends Equatable {
  const Summary(
      {@required this.message,
      @required this.global,
      @required this.countries});
  final String message;
  final Global global;
  final List<CountrySummary> countries;

  factory Summary.fromJson(dynamic json) {
    final List<CountrySummary> countries = List<CountrySummary>();
    if (json['Countries'] != null) {
      for (var item in json['Countries']) {
        countries.add(CountrySummary.fromJson(item));
      }
      countries.sort((a, b) => b.totalConfirmed.compareTo(a.totalConfirmed));
    }
    return Summary(
        message: json['Message'],
        global: Global.fromJson(json['Global']),
        countries: countries);
  }

  @override
  List<Object> get props => [message, global];
}
