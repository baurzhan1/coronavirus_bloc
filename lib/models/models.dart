library models;

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
part 'country_summary.dart';
part 'global.dart';
part 'summary.dart';
part 'country_details.dart';
