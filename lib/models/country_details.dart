part of models;

class CountryDetails extends Equatable {
  const CountryDetails(
      {@required this.name,
      @required this.date,
      @required this.confirmed,
      @required this.deaths,
      @required this.recovered,
      @required this.active,
      @required this.country});
  final String name;
  final String country;
  final DateTime date;
  final int confirmed;
  final int deaths;
  final int recovered;
  final int active;
  factory CountryDetails.fromJson(dynamic json) {
    return CountryDetails(
        name: json['Province'],
        date: DateTime.parse(json['Date']),
        confirmed: json['Confirmed'],
        deaths: json['Deaths'],
        recovered: json['Recovered'],
        active: json['Active'],
        country: json['Country']);
  }

  @override
  List<Object> get props =>
      [name, date, confirmed, deaths, recovered, active, country];
}
