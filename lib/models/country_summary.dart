part of models;

class CountrySummary extends Equatable {
  const CountrySummary(
      {@required this.name,
      @required this.slug,
      @required this.date,
      @required this.newConfirmed,
      @required this.newDeaths,
      @required this.newRecovered,
      @required this.totalConfirmed,
      @required this.totalDeaths,
      @required this.totalRecovered});

  final String name;
  final String slug;
  final DateTime date;
  final int newConfirmed;
  final int totalConfirmed;
  final int newDeaths;
  final int totalDeaths;
  final int newRecovered;
  final int totalRecovered;

  factory CountrySummary.fromJson(dynamic json) {
    return CountrySummary(
        name: json['Country'],
        slug: json['Slug'],
        date: DateTime.parse(json['Date']),
        newConfirmed: json['NewConfirmed'],
        newDeaths: json['NewDeaths'],
        newRecovered: json['NewRecovered'],
        totalConfirmed: json['TotalConfirmed'],
        totalDeaths: json['TotalDeaths'],
        totalRecovered: json['TotalRecovered']);
  }

  @override
  List<Object> get props => [
        name,
        slug,
        date,
        newConfirmed,
        totalConfirmed,
        newDeaths,
        totalDeaths,
        newRecovered,
        totalRecovered
      ];
}
