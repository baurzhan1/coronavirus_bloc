import 'package:flutter/material.dart';

extension CustomeColorScheme on ColorScheme {
  Color get confirmed => Colors.blue;
  Color get deaths => Colors.red;
  Color get recovered => Colors.green;
}
